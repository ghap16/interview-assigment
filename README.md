# InterviewAssigment

### Develop

Uses the default Django development server.

1. Build the images and run the containers:

    ```sh
    $ docker-compose up -d --build
    ```

2. Apply migrations:

    ```sh
    $ docker-compose run web python manage.py migrate
    ```

3. Load data group:
    ```sh
    $ docker-compose run web python manage.py loaddata group.json
    ```

    Test it out at [http://localhost:8000](http://localhost:8000). The "app" folder is mounted into the container and your code changes apply automatically.

### Production

Uses gunicorn + nginx.

1. Build the images and run the containers:

    ```sh
    $ docker-compose -f docker-compose.prod.yml up -d --build
    ```

    Test it out at [http://localhost:80](http://localhost:80). No mounted folders. To apply changes, the image must be re-built.