from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from rest_framework_nested import routers
from users.views import GroupList, UserViewSet, InfoView, \
    OrganizationViewSet, UserInOrganizationViewSet

# Get token (Login)
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
)

router = DefaultRouter(trailing_slash=False)
router.register(r'api/users', UserViewSet, basename='user')

routerO = routers.SimpleRouter()
routerO.register(r'api/organization', OrganizationViewSet, basename='organization')
organization_router = routers.NestedSimpleRouter(routerO, r'api/organization', lookup='organization')
organization_router.register(r'users', UserInOrganizationViewSet, basename='organization-users')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/auth/login/', TokenObtainPairView.as_view(), name='login'),
    path('api/auth/groups/', GroupList.as_view(), name='group-list'),
    url(r'^', include(routerO.urls)),
    url(r'^', include(organization_router.urls)),
    path('api/info/', InfoView.as_view(), name='info'),

]
urlpatterns += router.urls
