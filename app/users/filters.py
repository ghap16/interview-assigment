from rest_framework import filters


class FilterForOrganizationBackend(filters.BaseFilterBackend):
    """
    Filter user in self organization.
    """
    def filter_queryset(self, request, queryset, view):
        if request.user.organization:
            return queryset.filter(organization=request.user.organization)
        return False
