from django.contrib.auth.models import Group
from django.shortcuts import get_object_or_404
from rest_framework import generics, viewsets, filters, mixins
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend
from .models import User, Organization
from .serializers import GroupSerializer, UserSerializer, OrganizationSerializer, \
    UserShortSerializer, UserInfoSerializer
from .permissions import UserCRUDPermission, OrganizationRUPermission, UserInOrganizationPermission
from .filters import FilterForOrganizationBackend


class GroupList(generics.ListAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [IsAuthenticated]


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated, UserCRUDPermission]
    filter_backends = [
        FilterForOrganizationBackend,
        DjangoFilterBackend,
        filters.SearchFilter
    ]
    filterset_fields = ['phone']
    search_fields = ['name', 'email']


class OrganizationViewSet(mixins.RetrieveModelMixin,
                          mixins.UpdateModelMixin,
                          viewsets.GenericViewSet):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    permission_classes = [IsAuthenticated, OrganizationRUPermission]


class UserInOrganizationViewSet(mixins.RetrieveModelMixin,
                                mixins.ListModelMixin,
                                viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserShortSerializer
    permission_classes = [IsAuthenticated, UserInOrganizationPermission]
    filter_backends = [FilterForOrganizationBackend]

    def get_queryset(self):
        pk_organization = self.kwargs.get('organization_pk')
        organization = get_object_or_404(Organization, pk=pk_organization)
        return User.objects.filter(organization=organization)


class InfoView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        user = request.user
        serializer = UserInfoSerializer(user, context={'request': request})
        return Response(serializer.data)
