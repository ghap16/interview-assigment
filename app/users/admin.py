from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _

from .models import User, Organization


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    """Define admin model for custom User model with no email field."""

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('name', 'phone', 'birthdate', 'organization')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'name', 'phone', 'birthdate', 'is_staff')
    search_fields = ('email', 'name', 'phone')
    ordering = ('email',)


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    """Define admin model for Organization model."""
    list_display = ('name', 'phone')
    search_fields = ('name',)
    ordering = ('name',)
