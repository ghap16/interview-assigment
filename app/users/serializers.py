from django.contrib.auth.models import Group
from django.core import exceptions
from rest_framework import serializers
from django.contrib.auth import password_validation as validators
from .models import User, Organization


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['id', 'name']


class OrganizationRepresentationSerializer(serializers.BaseSerializer):
    def to_representation(self, instance):
        return {
            'id': instance.id,
            'name': instance.name
        }


class UserSerializer(serializers.ModelSerializer):
    organization = OrganizationRepresentationSerializer(read_only=True)

    class Meta:
        model = User
        fields = [
            'id',
            'email',
            'name',
            'phone',
            'birthdate',
            'organization',
            'password',
            'groups',
        ]
        extra_kwargs = {
            'password': {'write_only': True},
            'groups': {'write_only': True}
        }

    def get_fields(self, *args, **kwargs):
        """
        Si el metodo es POST el campo groups sera obligatorio.
        El campo groups es un list
        """
        fields = super(UserSerializer, self).get_fields(*args, **kwargs)
        request = self.context.get('request', None)
        if request and getattr(request, 'method', None) == "POST":
            fields['groups'].required = True
        return fields

    def _validate_password(self, user, password):
        errors = dict()
        try:
            validators.validate_password(password=password, user=user)
        except exceptions.ValidationError as e:
            errors['password'] = list(e.messages)
        if errors:
            raise serializers.ValidationError(errors)

    def validate(self, data):
        user = User(**data)
        password = data.get('password')
        if password:
            self._validate_password(user, password)
        return super(UserSerializer, self).validate(data)

    def create(self, validated_data):
        request = self.context.get('request')
        user = User(**validated_data)
        user.set_password(validated_data['password'])
        user.organization = request.user.organization
        user.save()
        return user


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = '__all__'


class UserShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'name']


class UserInfoSerializer(serializers.ModelSerializer):
    user_name = serializers.CharField(source='name')
    organization_name = serializers.CharField(source='organization.name')
    public_ip = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['user_name', 'id', 'organization_name', 'public_ip']

    def get_public_ip(self, obj):
        request = self.context.get('request')
        return request.META['REMOTE_ADDR']
