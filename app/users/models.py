from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator

phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")


class Organization(models.Model):
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=15, validators=[phone_regex])
    address = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    """User model."""

    username = None
    first_name = None
    last_name = None
    name = models.CharField(max_length=100, blank=True, null=True, default=None)
    phone = models.CharField(
        _('Phone number'),
        max_length=15, blank=True,
        null=True, default=None,
        validators=[phone_regex])
    email = models.EmailField(_('email address'), unique=True)
    birthdate = models.DateField(blank=True, null=True, default=None)
    organization = models.ForeignKey(
        'Organization',
        on_delete=models.SET_NULL,
        related_name='users',
        blank=True, null=True, default=None)

    USERNAME_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def has_group(self, group_name):
        """
        Los grupos son Administrator, Viewer, User
        """
        return self.groups.filter(name=group_name).exists()
