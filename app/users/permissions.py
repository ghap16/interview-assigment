from rest_framework import permissions


class UserCRUDPermission(permissions.BasePermission):
    """
    Verifica si el usuario cuenta con los permisos de acuerdo al grupo y metodo
    """
    def has_permission(self, request, view):
        user = request.user
        action = view.action
        if action == 'list':
            return user.has_group('Administrator') | user.has_group('Viewer')
        if action == 'create' or action == 'destroy':
            return user.has_group('Administrator')
        if action == 'retrieve':
            return user.has_group('Administrator') | user.has_group('Viewer') | user.__eq__(view.get_object())
        if action == 'partial_update':
            return user.has_group('Administrator') | user.__eq__(view.get_object())
        return False


class OrganizationRUPermission(permissions.BasePermission):
    """
    Verifica si el usuario cuenta con los permisos de acuerdo al grupo y metodo
    """
    def has_permission(self, request, view):
        user = request.user
        action = view.action
        if user.organization.__eq__(view.get_object()):
            if action == 'retrieve':
                return user.has_group('Administrator') | user.has_group('Viewer')
            if action == 'partial_update':
                return user.has_group('Administrator')
        return False


class UserInOrganizationPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.user
        action = view.action
        if action == 'list' or action == 'retrieve':
            return user.has_group('Administrator') | user.has_group('Viewer')
        return False
